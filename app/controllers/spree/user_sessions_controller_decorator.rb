module Spree
	Spree::UserSessionsController.class_eval do
		def create
			authenticate_spree_user!
	
			if spree_user_signed_in?
				respond_to do |format|
					format.html {
						flash[:success] = Spree.t(:logged_in_succesfully)
						redirect_back_or_default(after_sign_in_redirect(spree_current_user))
					}
					format.js {
						render json: { user: spree_current_user,
															ship_address: spree_current_user.ship_address,
															bill_address: spree_current_user.bill_address }.to_json
					}
				end
			else
				respond_to do |format|
					format.html {
						flash.now[:error] = t('devise.failure.invalid')
						render :new
					}
					format.js {
						render json: { error: t('devise.failure.invalid') }, status: :unprocessable_entity
					}
				end
			end
		end

		protected
		def after_sign_in_redirect(resource_or_scope)
			if resource_or_scope.customer_type == 'Vendor'
				admin_path
			else
				root_path
			end
		end	
	end
end