class AddCustomerTypeToSpreeUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :spree_users, :customer_type, :string, :default => "Vendor"
  end
end
